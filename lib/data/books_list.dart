import 'package:navigatortest/models/book_model.dart';

List<Book> books = [
  Book('first book', 'book1'),
  Book('second book', 'book2'),
  Book('third book', 'book3'),
];
